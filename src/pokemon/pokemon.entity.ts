import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class PokeEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  type: string;

  @Column()
  sexPics: string;

  @Column()
  pokePics: string;

  @Column()
  littlePokePics: string;

  @Column()
  pokeResum: string;
}
