import { PokeEntity } from './pokemon.entity';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { UpdateResult, DeleteResult } from 'typeorm';

@Injectable()
export class PokemonService {
  constructor(
    @InjectRepository(PokeEntity)
    private pokemonRepository: Repository<PokeEntity>,
  ) {}
  async findAll(): Promise<PokeEntity[]> {
    return await this.pokemonRepository.find();
  }

  async create(PokeEntity: PokeEntity): Promise<PokeEntity> {
    return await this.pokemonRepository.save(PokeEntity);
  }

  async update(PokeEntity: PokeEntity): Promise<UpdateResult> {
    return await this.pokemonRepository.update(PokeEntity.id, PokeEntity);
  }

  async delete(id): Promise<DeleteResult> {
    return await this.pokemonRepository.delete(id);
  }
}
