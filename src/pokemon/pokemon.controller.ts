import { PokemonService } from './pokemon.service';
import { PokeEntity } from './pokemon.entity';
import { Controller, Get } from '@nestjs/common';
import { Post, Put, Delete, Body, Param } from '@nestjs/common';

@Controller('pokemon')
export class PokemonController {
  constructor(private Service: PokemonService) {}

  @Get()
  index(): Promise<PokeEntity[]> {
    return this.Service.findAll();
  }
  @Post('create')
  async create(@Body() PokeData: PokeEntity): Promise<any> {
    return this.Service.create(PokeData);
  }
  @Put(':id/update')
  async update(@Param('id') id, @Body() pokeData: PokeEntity): Promise<any> {
    pokeData.id = Number(id);
    console.log('Update #' + pokeData.id);
    return this.Service.update(pokeData);
  }
  @Delete(':id/delete')
  async delete(@Param('id') id): Promise<any> {
    return this.Service.delete(id);
  }
}
