import { PokeEntity } from './pokemon.entity';
import { Module } from '@nestjs/common';
import { PokemonController } from './pokemon.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PokemonService } from './pokemon.service';

@Module({
  imports: [TypeOrmModule.forFeature([PokeEntity])],
  providers: [PokemonService],
  controllers: [PokemonController],
})
export class PokemonModule {}
